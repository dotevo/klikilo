// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const exec = require('child_process').exec;
window.$ = window.jQuery = require('jquery');

const Android = require('./android')

let android = new Android()


var canvas=document.getElementById("canvas");
var context=canvas.getContext('2d');

let width = 0;
let height = 0;

function resize() {
    var w = $(window).width();
    var h = $(window).height() - $("#header").height();

    $("#canvas").attr("width", w + "px");
    $("#canvas").attr("height", h + "px");

    $("#canvas").css("width", w + "px");
    $("#canvas").css("height", h + "px");
}

$(window).bind("resize", resize);
resize();

let displayId = 0

// Screenshots
function getScreenShot() {
    android.getScreenShot(displayId).then((output) => {
        drawing = new Image()
        drawing.src = output + '?' + new Date().getTime()
        drawing.onload = function() {
            width = drawing.naturalWidth
            height = drawing.naturalHeight
            console.log(width + "  " + height)
            context.drawImage(drawing,0,0,$("#canvas").width(),$("#canvas").height())
        }
    }, (error) => {})
}

let timer = null;
function setReloader(val) {
    if (timer != null)
        clearInterval(timer);
    timer = setInterval(getScreenShot, val);
}
setReloader($("#reload_val").val());
$("#reload_val").bind('keyup mouseup', function () {
    console.log("Timer changed")
    setReloader($(this).val())
});
///////////////////////////////////////////////
//Display
function reload() {
    android.getDisplays().then(displays => {
        if(displays == null) return;
        $("#display").find('option').remove()
        for(let p of displays) {
            $("#display").append('<option value="' + p.displayId + '">' + p.displayId + ': ' + p.name + '</option>')
        }
    })    
}

reload()
$("#display_reload").on("click", reload);

$("#display").change(function() {
    console.log($(this).val())
    displayId = $(this).val()
})

function getXY(event) {
    var x = width * (event.pageX - this.offsetLeft) / $("#canvas").width()
    var y = height * (event.pageY - this.offsetTop) / $("#canvas").height()
    return [x, y];
}

let startX, startY
let startTime = 0
$("#canvas")
  .mousedown(function(event) {
    [startX, startY] = getXY.call(this, event)
    var d = new Date()
    startTime = d.getTime()
    console.log(startTime)
  })
  .mouseup(function(event) {
    [x, y] = getXY.call(this, event)
    var d = new Date()
    const time = d.getTime() - startTime;
    console.log(time)
    if(time > 200 && (Math.abs(x-startX) > 10 ||  Math.abs(y-startY) > 10)) {
        console.log('swipe x: ' + x + ' y: ' + y)
        android.sendSwipe(displayId, Math.floor(startX), Math.floor(startY), Math.floor(x), Math.floor(y), time)
    } else {
        [x, y] = getXY.call(this, event)
        console.log('click x: ' + x + ' y: ' + y)
        android.sendTap(displayId, x, y)
    }
    console.log(event)
  });
