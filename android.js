const exec = require('child_process').exec;

const regex = new RegExp(/mDisplayId=([\d]+)[\w=\-@\s#)(,\[\]]*mBaseDisplayInfo=DisplayInfo{"([\w\s#@_\-]*)/,'g')

function run(cmd) {
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) return reject(error)
            if (stderr) return reject(stderr)
            resolve(stdout)
        }, () => {return reject()})
    })
}

class Android {
    constructor() {
        this.serialNb = null
        this.screenShotOngoing = false
    }

    get serial() {
        return this.serialNb
    }

    set serial(serial) {
        this.serialNb = serial 
    }

    getDisplays() {
        return run('adb shell dumpsys display').then(output => {
            let matches;
            let ret = [];
            do {
                matches = regex.exec(output)
                if (matches) {
                    ret.push({'displayId':matches[1], 'name':matches[2]})
                }
            } while(matches);
            return ret;
        }, () => {})
    }

    getScreenShot(displayId = 0, location = '/tmp/klikilo-' + displayId + '.png') {
        if(this.screenShotOngoing) return Promise.reject("Ongoing...") 
        this.screenShotOngoing = true

        const cmd = 'adb shell screencap -p' + (displayId > 0 ? ' -d ' + displayId : '') + ' > ' + location;
        console.log(cmd);
        return run(cmd).then(() => {
            this.screenShotOngoing = false
            return location
        }, () => {this.screenShotOngoing = false})
    }

    sendTap(displayId, x, y) {
        const cmd = 'adb shell input  ' +  (displayId > 0 ? ' -d ' + displayId : '') + ' tap ' + x + ' ' + y
        console.log(cmd);
        return run(cmd)
    }

    sendSwipe(displayId, x, y, x2, y2, duration) {
        const cmd = 'adb shell input  ' +  (displayId > 0 ? ' -d ' + displayId : '') + ' swipe ' + x + ' ' + y +
            ' ' + x2 + ' ' + y2 + ' ' + duration
        console.log(cmd);
        return run(cmd)
    }
}

module.exports=Android;
